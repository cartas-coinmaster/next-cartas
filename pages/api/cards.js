import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export default async (req, res) => {
  const method = req.method  
  
  switch(method) {
    case 'GET':
      const cards = await prisma.card.findMany({
        where:{
          aproved:true
        },
        include:{
          group:true
        }
      })
      res.statusCode = 200
      res.json(cards)
      break
    default:
      res.statusCode = 200
      res.json({
        error:'Método não implementado!'
      })
      break
  }
  
}
