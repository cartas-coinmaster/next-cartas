import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export default async (req, res) => {
  const method = req.method  
  
  switch(method) {
    case 'GET':
      const groups = await prisma.group.findMany({
      })
      res.statusCode = 200
      res.json(groups)
      break
    default:
      res.statusCode = 200
      res.json({
        error:'Método não implementado!'
      })
      break
  }
  
}
