import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export default async (req, res) => {
  const method = req.method  
  const data = req.body

  switch(method) {
    case 'GET':
      const requests = await prisma.request.findMany({
        include:{
          group:true,
          card:true
        }
      })
      res.statusCode = 200
      res.json(requests)
      break
    case 'POST':
      const request = await prisma.request.create({
        data:data
      })
      res.statusCode = 200
      res.json(request)
      break
    default:
      res.statusCode = 200
      res.json({
        error:'Método não implementado!'
      })
      break
  }
  
}
